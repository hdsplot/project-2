'use strict'

const { task, parallel, watch, series, src, dest } = require('gulp');

const sass = require('gulp-sass')(require('sass'));
const htmlmin = require('gulp-htmlmin');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
const cleanCss = require('gulp-clean-css');
const concat = require('gulp-concat');
const terser = require('gulp-terser');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const gulpif = require('gulp-if');
const yargs = require('yargs');
const argP = yargs.argv,
    production = !!argP.production;


task('serve', () => {
    return browserSync.init({
        server: {
            baseDir: ('./dist')
        },
        port: 9000,
        open: true
    });
});


task('html', () => {
    return src('./src/index.html')
        .pipe(gulpif(production, htmlmin({ collapseWhitespace: true })))
        .pipe(browserSync.stream())
        .pipe(dest('./dist'));
})

task('sass', () => {
    return src('./src/scss/styles.scss')
        .pipe(sass().on("error", sass.logError))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 version'],
            grid: true,
        }))
        .pipe(gulpif(production, cleanCss()))
        .pipe(gulpif(production, rename({ suffix: ".min" })))
        .pipe(browserSync.stream())
        .pipe(dest('./dist/css/'));
})

task('js', () => {
    return src('./src/js/*.js')
        .pipe(concat('main.min.js'))
        .pipe(gulpif(production, terser()))
        .pipe(browserSync.stream())
        .pipe(dest('./dist/js/'));
})

task('img', () => {
    return src('./src/img/**/*')
        .pipe(imagemin())
        .pipe(browserSync.stream())
        .pipe(dest('./dist/img'));
})

task('cleanDist', () => {
    return src('./dist', { allowEmpty: true }).pipe(clean())
})

task('watch', () => {
    watch('./src/index.html', parallel('html'));
    watch('./src/scss/**/*.scss', parallel('sass'))
    watch('./src/js/**/*.js', parallel('js'));
    watch('./src/img/**/*', series('img'));
    watch('./dist/**/*.html').on('change', browserSync.reload);
})
task('build', series('cleanDist', series('html', 'js', 'img', 'sass')));
task('dev', series('build', parallel('serve', 'watch')));